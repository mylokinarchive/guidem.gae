import sys
sys.path[0:0] = ['distlib', 'distlib.zip']

import google.appengine.ext.bulkload.transform as transform
import base64
import models

def get_or_create_composer(name):
    composer = models.Composer.all().filter('name =', name).get()
    if not composer:
        composer = models.Composer(name=name)
        composer.put()
    return composer.key()


if __name__=='__main___':
    print 'create csv file'
