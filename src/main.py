import sys, os
sys.path[0:0] = ['distlib', 'distlib.zip']


from pyramid.config import Configurator
from google.appengine.ext.webapp import util

settings = {
    'reload_templates':'true',
    'debug_templates':'true',

    'mako.directories': 'templates/',
    'mako.input_encoding': 'utf-8',
    'mako.strict_undefined': False,

}

if __name__=='__main__':
    config = Configurator(settings=settings)

    config.include('pyramid_handlers')
    config.add_handler('home', '/', handler='views.Basic', action='home')
    config.add_handler('download', '/download/{id}', handler='views.Basic', action='download')
    config.add_handler('basic', '/{action}', handler='views.Basic')
    config.add_handler('radio', 'radio/{action}', handler='views.radio.Radio')

    app = config.make_wsgi_app()
    util.run_wsgi_app(app)
