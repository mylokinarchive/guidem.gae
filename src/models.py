from google.appengine.ext import db
from google.appengine.ext.mapreduce import operation as op
from aetycoon import DerivedProperty
import boto

class Composer(db.Model):
    name = db.StringProperty()

    @property
    def image(self):
        pass

class Composition(db.Model):
    title = db.StringProperty()
    s3key = db.StringProperty()
    composer = db.ReferenceProperty(Composer)

    @DerivedProperty
    def scan(self):
        return u'%s %s'%(self.composer.name, self.title)

    @property
    def link(self):
        conn = boto.connect_s3('AKIAJ5TBK4KX3L3HJE3A','KlMgimVql5cTAuRMp8jdCrvWr+9Q7+tOEzkO9wam')
        return conn.generate_url(3600, 'GET', 'guidem', self.s3key, force_http=True)

    @staticmethod
    def process(entity):
        yield op.counters.Increment('count')

    @staticmethod
    def clean(entity):
        yield  op.db.Delete(entity)

class Counter(db.Model):
    count = db.IntegerProperty(indexed=False)


