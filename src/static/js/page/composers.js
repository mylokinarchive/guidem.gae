window.resultOffset = 0;
window.search = '';
var per_page = 10;

function renderTrack(title, link){
    element = $('#template > .compositionBox').first().clone();
    element.children('h5').children('.localComposition').text(title);
    element.children('h5').children('.localComposition').attr('href', link);
    return element
};

function renderComposer(name){
    element = $('#template > .composerBox').first().clone();
    element.children('.localName').text(name);
    return element;
};

var nextCommand = {
    'search': '',
    'offset': 0
};

function renderNext(){
    element = $('#template > .nextBox').first().clone();
    element.removeClass('hidden');
    return element;
};

function renderCompositions(compositions){
    $.each(compositions, function(index, composition){
        element = renderTrack(composition[0], composition[1]);
        element.appendTo($('#compositions'));
        element.fadeIn();
    });
};

function xhrError(){
    message('Произошла ошибка.<br/>Пожалуйста, повторите запрос позже.');
    $('#composer > .composerBox .loader').hide();
};


var cached_compositions = new Array();
function bigsearch(search, offset, next){
    window.loader.show();
    $.getJSON(window.url_, {'search':search, 'offset':offset}, function(compositions){
        if((compositions.length==0)&&(!next)){
            message('Ничего не найдено.<br/> Попробуйте изменить запрос.')
        };
        window.loader.hide();
        renderCompositions(compositions.slice(0, per_page));
        if(compositions.length>per_page){
            cached_compositions = compositions;
            renderNext().appendTo($('#compositions'));
        };
    }).error(xhrError);
};

function nextPage(offset){
    if(cached_compositions.slice(offset, offset+per_page).length==per_page){
        renderCompositions(cached_compositions.slice(offset, offset+per_page));
        renderNext().appendTo($('#compositions'));
    } else
        bigsearch(nextCommand.search, nextCommand.offset, true)
};


$(document).ready(function(){
    window.url_ = '';

    $('.actionLoadComposition').click(function(){
        window.url_ = $(this).attr('href');
        var composer = $(this).prev('h4').text();
        $('#compositions > *').fadeOut().remove();
        if($('#composer').html()){
            $('#composer').children('.composerBox').children('.localName').text(composer);
        }else{
            $('#composer').html(renderComposer(composer)).show();
        }
        window.loader =  $('#composer > .composerBox .loader');
        loader.show();
        nextCommand.search = window.search;
        nextCommand.offset = 0;
        bigsearch('', 0);
        return false;
    });
    $('#search-input').keyup(function(){
        search = $(this).val();
    });
    $('#search-form').submit(function(){
        $('#compositions > *').remove();
        loader.show();
        nextCommand.offset = 0;
        bigsearch(window.search, nextCommand.offset)
        return false;
    });
    $('.nextCommand').click(function(){
        $('#compositions > *').remove();
        nextCommand.offset+=per_page;
        nextPage(nextCommand.offset);
        return false;
    });

    $(window).scroll(function(){
        var top = $('#scroller').offset().top;
        if(top>180)
            $('#scroll').css('margin-top', top-180);
        else
            $('#scroll').css('margin-top', 0);
    });
})