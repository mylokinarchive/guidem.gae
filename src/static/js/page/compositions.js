var per_page = 20;

function renderTrack(title, artist, link){
    element = $('#template > .trackBox').first().clone();
    element.children('.localArtist').text(artist);
    element.children('.localTitle').text(title);
    element.children('.localTitle').attr('href', link);
    return element
};

function renderCompositions(compositions){
    $.each(compositions, function(index, composition){
        element = renderTrack(composition[0], composition[1],  composition[2]);
        element.appendTo($('#resultBox'));
        element.fadeIn();
    });
    $('.trackBox').last().addClass('lastTrackBox');
};

var nextCommand = {
    'search': '',
    'offset': 0,
}

function renderNext(){
    element = $('#template > .nextBox').first().clone();
    return element;
};

function xhrError(){
    loader.hide();
    message('Произошла ошибка.<br/>Пожалуйста, повторите запрос позже.');
    $('#composer > .composerBox .loader').hide();
};

var cached_compositions = new Array();
function bigsearch(search, offset){
    loader.show();
    $.getJSON(url, {'search':search, 'offset':offset}, function(compositions){
        if(compositions.length==0){
            message('Ничего не найдено.<br/> Попробуйте изменить запрос.')
        };
        loader.hide();
        renderCompositions(compositions.slice(0, per_page));
        if(compositions.length>per_page){
            cached_compositions = compositions;
            renderNext().appendTo($('#resultBox'));
        };
    }).error(xhrError);
};

function nextPage(offset){
    console.log(offset);
    renderCompositions(cached_compositions.slice(offset, offset+per_page));
    if(cached_compositions.slice(offset+per_page).length)
        renderNext().appendTo($('#resultBox'));
};

$(document).ready(function(){
    window.loader = $('.searchBox > .loader');
    window.url = $("#big-search-form").attr('action');

    $('#big-search-submit').click(function(){
        $('#big-search-form').submit();
    });

    $("#big-search-form").submit(function(){
        var search = $('#big-search-input').val();
        if(search==''){
            message('Пожалуйста введите запрос.')
            return false;
        }
        $('#resultBox > *').remove();
        nextCommand.search = search;
        nextCommand.offset = 0;
        bigsearch(search, window.resultOffset);
        return false;
    });
    $('.nextCommand').click(function(){
        $('#resultBox > *').remove();
        nextCommand.offset+=per_page;
        nextPage(nextCommand.offset);
        return false;
    });

})