
var artists = new Array();
var playByComposition = false;

function animateHighlight(e){
    $(e).hover(function(){
        $(e).animate({'opacity':1}, 'slow');
    }, function(){
        $(e).animate({'opacity':0.7}, 'fast');
    });
}
$(document).ready(function(){

    $('.artistTag').click(function(){
        $(this).toggleClass('activeArtistTag');
        if($(this).hasClass('activeArtistTag')){
            artists.push($(this).children('.artistTagId').text());
        }else{
            idx = artists.indexOf($(this).children('.artistTagId').text());
            artists.splice(idx, 1);
        };
        if($('.activeArtistTag').length){
            $('#cancelSelection').animate({'opacity':1});
            $('#cancelSelection').removeClass('cancelSelectionDisabled');
        }else{
            $('#cancelSelection').animate({'opacity':0.3});
            $('#cancelSelection').addClass('cancelSelectionDisabled');

        };
    });
    $('#compositionPlayLink').click(function(){
        $(this).toggleClass('composition');
        playByComposition = $(this).hasClass('composition');
        return false;
    });
    $('#cancelSelection').click(function(){
        $('#cancelSelection').animate({'opacity':0.3});
        $('#cancelSelection').addClass('cancelSelectionDisabled');
        $('.activeArtistTag').removeClass('activeArtistTag');
        artists = new Array();
        return false;
    });
    animateHighlight('#playImg');
    animateHighlight('#ffImg');

    sm.onready(function (){
        api.playRandomTrack();
    });

});
