import sys
sys.path[0:0] = ['distlib', 'distlib.zip']

#from google.appengine.api import taskqueue
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

from google.appengine.ext.mapreduce.model import MapreduceState
import models

class CompositionsCountCallback(webapp.RequestHandler):
    def post(self):
        job_id = self.request.get('job_id') or self.request.headers.get('Mapreduce-Id')
        if not job_id:
            return
        state = MapreduceState.get_by_job_id(job_id)
        count = state.counters_map.get('count')
        key = models.Counter(key_name='compositions_counter')
        key.count = count
        key.put()

def main():
    run_wsgi_app(webapp.WSGIApplication([
        ('/task/queue/compositions_count', CompositionsCountCallback),
    ]))

if __name__ == '__main__':
    main()
