<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
 <title> ${self.title()} | Guidem </title>

 <link rel="stylesheet" type="text/css" href="/css/design.css" />
 <link rel="stylesheet" type="text/css" href="/css/radio.css" />
 <!--[if lte IE 7]>
  <link rel="stylesheet" type="text/css" href="/css/designie.css" />
 <![endif]-->

 <link rel="stylesheet" type="text/css" href="/sm2/flashblock.css" />
 <script type="text/javascript" src="/sm2/soundmanager2.js"></script>
 <script type="text/javascript" src="/js/lib/jquery.js"></script>
 <script type="text/javascript" src="/js/base.js"></script>
 <script type="text/javascript" src="/js/radio.js"></script>

 ${self.js()}

 <meta http-equiv="content-type" content="text/html; charset=utf-8" />

 <link rel="icon" href="/images/favicon/logo.png" type="image/png" />
 <link rel="shortcut icon" href="/images/favicon/logo.ico" />

</head>

<body>

<ul id="navigation">
  <li>
    <a href="/" id="logo-box"><img id="logo" src="/images/design/logo.png" /></a>
  </li>
  <li>
    <a href="${request.route_url('basic', action='composers')}"><strong>Композиторы</strong></a>
  </li>
  <li>
    <a href="${request.route_url('basic', action='compositions')}"><strong>Композиции</strong></a>
  </li>
</Ul>


<div id="contenttop"></div>
<div id="main">

 <div id="content">
   <div id="sm2-container-img" align="left"><img src="/images/header/herror.png" /></div>
   <div id="sm2-container">
     <h3>Пожалуйста активируйтей flash-плагин, расположенный ниже</h3>
   </div>
   ${next.body()}
 </div>

 <div id="sidebar">
   ${self.sidebar()}
 </div>

</div>

<div id="contentbottom"> </div>

<div id="footer">
 <span id="year"> © 2010-2011 </span>
 <span id="label"> Guidem </span>
 <br />
 <span id="license">
   Напоминаем, что все размещенные материалы предназначены исключительно для ознакомительных целей.
 </span>
</div>

<div id="popup">
  <div id="popup-close">
    <img id="popup-close-icon" src="/images/widget/close.gif" alt="Close message" />
  </div>
  <span id="popup-message">&nbsp;</span>
</div>

<script type="text/javascript"><!--
reformal_wdg_domain    = "guidem";
reformal_wdg_mode    = 0;
reformal_wdg_title   = "Guidem: Музыкальный гид";
reformal_wdg_ltitle  = "";
reformal_wdg_lfont   = "";
reformal_wdg_lsize   = "";
reformal_wdg_color   = "#bfbfbf";
reformal_wdg_bcolor  = "#516683";
reformal_wdg_tcolor  = "#FFFFFF";
reformal_wdg_align   = "right";
reformal_wdg_charset = "utf-8";
reformal_wdg_waction = 0;
reformal_wdg_vcolor  = "#9FCE54";
reformal_wdg_cmline  = "#E0E0E0";
reformal_wdg_glcolor  = "#105895"
reformal_wdg_tbcolor  = "#FFFFFF"
//-->
</script>

<script type="text/javascript" language="JavaScript" src="http://widget.reformal.ru/tab5.js"></script><noscript><a href="http://guidem.reformal.ru">Guidem: Музыкальный гид feedback </a> <a href="http://reformal.ru"><img src="http://reformal.ru/i/logo.gif" /></a></noscript>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-10571639-1");
pageTracker._trackPageview();
} catch(err) {}</script>


<!-- Yandex.Metrika -->
<script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<div style="display:none;"><script type="text/javascript">
try { var yaCounter1922239 = new Ya.Metrika(1922239);
yaCounter1922239.clickmap(true);
yaCounter1922239.trackLinks(true);
} catch(e){}
</script></div>
<noscript><div style="position:absolute"><img src="//mc.yandex.ru/watch/1922239" alt="" /></div></noscript>
<!-- /Yandex.Metrika -->



</body>

<%def name="title()"></%def>
<%def name="js()"></%def>
