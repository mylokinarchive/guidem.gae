<%inherit file="/base.mako" />

<div>
  <img src="/images/header/hcomposers.png" />
</div>

<section>
  %for composer in composers:
  <div class="composer">
    <h4>${composer.name}</h4>
    %if composer.image:
    <img src="/images/header/hcomposers.png" />
    %endif
    <a class="actionLoadComposition" onclick="javacript: return false;" href="${request.route_url('radio', action='composer_compositions')}?composer=${composer.key().id()}">Композиции</a>
  </div>
  %endfor
</section>

<%def name="sidebar()">
<div id="scroller"></div>
<div id="scroll">
<section id="template" class="hidden">
  <div class="compositionBox hidden">
    <h5>
      <a target="_blank" class="localComposition"></a>
    </h5>
  </div>
  <div class="nextBox hidden">
    <a class="nextCommand">Далeе...</a>
  </div>
  <div class="composerBox">
    <img class="hrImage" src="/images/header/hrcomposer.png" />
    <hr class="space" />
    <h4 class="localName"></h4>
    <form id="search-form" action="" method="get">
      <div id="search">
        <input name="query" id="search-input" type="text" />
        <input id="search-icon" type="submit" value=""/>
      </div>
    </form>
    <hr class="space" />
    <div class="loader">
      <img src="/images/widget/ajax-loader.gif" />
    </div>

  </div>
</section>
<div>
  <section id="composer"></section>
  <section id="compositions"></section>
</div>
</div>
</%def>

<%def name="title()">Композиторы</%def>
<%def name="js()">
 <script type="text/javascript" src="/js/page/composers.js"></script>
</%def>
