<%inherit file="/base.mako" />

<div id="template" class="hidden">
  <li class="trackBox">
    <a class="localArtist trackBoxArtist" target="_blank">&nbsp;</a>
    <span> &nbsp;&nbsp;&nbsp; </span>
    <a class="localTitle trackBoxTitle" href="#">&nbsp;</a>
  </li>
  <div class="nextBox prepend-top">
    <a class="nextCommand">Далeе...</a>
  </div>
</div>

<img src="/images/header/hcompositions.png" />
<hr class="space" />
<div class="searchBox first">
  <form id="big-search-form" action="${request.route_url('radio', action='compositions_search')}">
    <div id="big-search">
      <div id="big-search-wrap">
        <input id="big-search-input" type="text" />
      </div>
      <div id="big-search-submit">
        Поиск
      </div>
    </div>
  </form>
  <hr class="space" />
  <div class="loader hidden">
    <img src="/images/widget/ajax-loader.gif" />
  </div>

</div>

<ul class="first" id="resultBox">
</ul>

<%def name="sidebar()"></%def>
<%def name="title()">Композиции</%def>
<%def name="js()">
 <script type="text/javascript" src="/js/page/compositions.js"></script>
</%def>
