<%inherit file="/base.mako" />

<div align="left"><img src="/images/header/hplayer.png" /></div>
<hr class="space" />
<table class="first" border="0">
  <tr>
    <td id="playBox"> <img id="playImg" src="/images/widget/play.png" /> </td>
    <td id="ffBox"> <img id="ffImg" src="/images/widget/fastforward.png" /> </td>
    <td id="description">
      <h3 id="descriptionTitle"> &nbsp; </h3>
      <h4 id="descriptionArtist"> &nbsp; </h4>
    </td>

    <td id="volumeBox">
      <div id="volumeBarBox">
	      <div id="volumeBar">&nbsp;</div>
	      <div id="volumeBarBg">&nbsp;</div>
	      <div id="volumeBarCtrl">&nbsp;</div>
      </div>
      <div id="volumeImgBox"><img id="volumeImg" src="/images/widget/volume.png" /></div>
    </td>

  </tr>
  <tr>
    <td colspan="4">
      <div id="progressbarBox">
        <div id="pgbPlay"> &nbsp; </div>
        <div id="pgbLoading"> &nbsp; </div>
        <div id="pgbBlank"> &nbsp; </div>
        <div id="pgbRewinding"> &nbsp; </div>
      </div>
    </td>
  </tr>
  <tr>
    <td id="playCounter" colspan="1"> 0:00 </td>
    <td id="totalCounter" colspan="3"> 0:00 </td>
  </tr>
</table>
<hr class="space" />
<div align="left"><img src="/images/header/hhistory.png" /></div>
<hr class="space" />
<ul class="first" id="historyBox">

  <li class="trackBox trackBoxHidden">
    <img class="trackPlayImg" src="/images/widget/play.png" /> &nbsp;
    <div class="trackBoxId"></div>
    <a class="trackBoxArtist" target="_blank">&nbsp;</a>
    <span> &nbsp;&nbsp;&nbsp; </span>
    <a class="trackBoxTitle" href="#" target="_blank">&nbsp;</a>
  </li>

</ul>


<%def name="sidebar()">
<div align="right"><img src="/images/header/hrcomposers.png" /></div>
<hr class="space" />
<div id="cancelSelectionBox">
  <a id="cancelSelection" class="cancelSelectionDisabled" href="#">Отменить выделение</a>
</div>
<hr class="space" />

<div id="tagCloud">
  %for artist in composers:
  <a href="#" class="prior1 artistTag">${artist.name}<span class="artistTagId">${artist.key().id()}</span></a><br />
  %endfor
</div>
</%def>
<%def name="title()">Радио классической музыки</%def>
<%def name="js()">
<script type="text/javascript" src="/js/page/radio.js"></script>
</%def>

