from pyramid_handlers import action
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
import models

class Basic(object):
    def __init__(self, request):
        self.request = request

    @action(renderer='radio.mako')
    def home(self):
        composers = models.Composer.all().order('name')
        return {'composers':composers}

    @action(renderer='composers.mako')
    def composers(self):
        composers = models.Composer.all().order('name')
        return {'composers':composers}

    @action(renderer='compositions.mako')
    def compositions(self):
        return {}

    @action()
    def download(self):
        try:
            id_ = int(self.request.matchdict['id'])
        except ValueError:
            return Response('Wrong composition', 404)
        composition = models.Composition.get_by_id(id_)
        if composition:
            return HTTPFound(location=composition.link)
        else:
            return Response('Composition not found', 404)
