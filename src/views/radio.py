from pyramid_handlers import action
from pyramid.response import Response

import models
from random import randint
import itertools

LIMIT = 101
SEARCH_LIMIT = 100

class Radio(object):
    def __init__(self, request):
        self.request = request

    @action(renderer='json', xhr=True)
    def composition(self):
        composers = self.request.params.get('composers')
        if composers:
            composers = map(lambda c: models.Composer.get_by_id(int(c)), composers.split(','))
            compositions = list(itertools.chain(*map(lambda c:list(models.Composition.all().filter('composer =', c)), composers)))
            offset = randint(0, len(compositions))
            composition = compositions[offset]
        else:
            count = models.Counter.get_by_key_name('compositions_counter').count
            offset = randint(0, count-1)
            composition = models.Composition.all().fetch(1, offset)[0]
        return {
            'id': str(composition.key()),
            'artist': composition.composer.name,
            'title': composition.title,
            'url': composition.link,
            'track_url': composition.link,
            }

    @action(renderer='json', xhr=True)
    def composer_compositions(self):
        try:
            composer = int(self.request.params.get('composer', -1))
            offset = int(self.request.params.get('offset', 0))
        except:
            return Response('Wrong composer', 404)
        composer = models.Composer.get_by_id(composer)
        if composer:
            search = self.request.params.get('search')
            query = models.Composition.all().filter('composer =', composer).order('title')
            if search:
                normalize = lambda string: (unicode(string, 'utf-8') if type(string)==str else string).lower()
                search = normalize(search)
                def _filter(query):
                    for composition in query:
                        composition_name = normalize(composition.title)
                        for subsearch in search.split():
                            if subsearch in composition_name:
                                continue
                            else:
                                break
                        else:
                            yield composition
                query = _filter(query)
            query = list(query)
            if offset:
                query = query[offset:]
            query = query[:LIMIT]
            link = lambda obj:self.request.route_url('download', id=obj.key().id())
            compositions = map(lambda c: (c.title, link(c)), \
                query)
            return compositions
        else:
            return Response('Composer not found', 404)

    @action(renderer='json', xhr=True)
    def compositions_search(self):
        try:
            offset = int(self.request.params.get('offset', 0))
        except:
            return Response('Wrong offset', 404)
        search = self.request.params.get('search')
        query = models.Composition.all().order('title')
        if not search:
            return Response('Empty query', 403)
        normalize = lambda string: (unicode(string, 'utf-8') if type(string)==str else string).lower()
        search = normalize(search)
        def _filter(query):
            for composition in query:
                composition_name = normalize(composition.scan)
                for subsearch in search.split():
                    if subsearch in composition_name:
                        continue
                    else:
                        break
                else:
                    yield composition
        query = list(_filter(query))
        if offset:
            query = query[offset:]
        query = query[:SEARCH_LIMIT]
        link = lambda obj:self.request.route_url('download', id=obj.key().id())
        compositions = map(lambda c: (c.title, c.composer.name, link(c)), \
                query)
        return compositions


